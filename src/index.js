const HistoryScanner = require('./history-scanner');
const Formatter = require('./formatter');
const Utils = require('./utils');

let scanner = new HistoryScanner();

let team = [
    'jsek'
]

////////////////////////////////////////////////////////////////////////
// Example 1
// Log commits matching PR pattern within 10 commits limit
//
let example_1 = () =>
    scanner.scan({
        limit: 10,
        filter: c => Utils.isPullRequestMerge(c)
    })
    .then(Formatter.asCommitsTable)
    .catch(Formatter.printError);

////////////////////////////////////////////////////////////////////////
// Example 2
// Log commits from given date range with additional statistics
//
let example_2 = () =>
    scanner.scan({
        limit: 15000,
        until: new Date('2016-09-08 17:50'),
        since: new Date('2016-09-08 16:48'),
        filter: c => Utils.isPullRequestMerge(c)
    })
    .then(commits => scanner.withMergedCommits(commits))
    .then(Utils.reverse)
    .then(Formatter.asCommitsTable)
    .catch(Formatter.printError);

////////////////////////////////////////////////////////////////////////
// Example 3
// Log commits from given month with statistics + filtered for a team
//
let example_3 = () =>
    scanner.scanMonth({
        limit: 14000,
        month: '2017-02',
        filter: c => Utils.isPullRequestMerge(c) && team.includes(c.author().name())
    })
    .then(commits => scanner.withMergedCommits(commits))
    .then(Utils.reverse)
    .then(Formatter.asCommitsTable)
    .then(Formatter.printSummary('2017-02'))
    .catch(Formatter.printError);

////////////////////////////////////////////////////////////////////////
// Example 4
// Log statistics for given months
//
let example_4 = () =>
    Promise.all([
            '2016-08',
            '2016-09',
            '2016-10',
            '2016-11',
            '2016-12',
            '2017-01',
            '2017-02'
        ]
        .map(month =>
            scanner.scanMonth({
                month,
                limit: 14000,
                filter: c => Utils.isPullRequestMerge(c) // && team.includes(c.author().name())
            })
            .then(commits => scanner.withMergedCommits(commits))
            .then(Formatter.monthSummary(month))
        ))
    .then(Formatter.asCompactTable)
    .catch(Formatter.printError);

////////////////////////////////////////////////////////////////////////
// Demo
//
example_1();
// example_2();
// example_3();
// example_4();