const chalk = require('chalk');
const Table = require('cli-table');
const format = require('fmt-obj');
const moment = require('moment');

const MESSAGE_LIMIT = 120;
const SHA_LIMIT = 7;
const DATE_FORMAT = 'LL';

let countIndividualWork = (commits) => {
    if (commits.length === 0) return 0;
    let individualPullRequestsCount = commits.reduce(((o, x) => o + (x.authorsCount === 1 ? 1 : 0)), 0);
    return parseFloat((100 * individualPullRequestsCount / commits.length).toFixed(2));
}

class Formatter {

    static printError(err) {
        console.error(chalk.bold.red(err.stack));
    }

    static asCompactTable(objectsArray) {
        let keys = Object.keys(objectsArray[0]);
        let table = new Table({
            head: keys,
            style: {
                compact: true,
                'padding-left': 1
            }
        });
        table.push(...objectsArray.map(x => keys.map(k => x[k])));
        console.log(table.toString());
        return objectsArray;
    }

    static asCommitsTable(commits) {
        let commitsData = commits.map(c => ({
            'SHA': chalk.yellow(c.sha().slice(0, SHA_LIMIT)),
            'Date': chalk.magenta(moment(c.date()).format(DATE_FORMAT)),
            'Merger': chalk.blue(c.author().name()),
            'Message': c.message().split('\n')[0].slice(0, MESSAGE_LIMIT)
        }));

        if (commits.length > 0 && commits[0].mergedCount !== undefined) {
            commits.forEach(c => Object.assign(commitsData[commits.indexOf(c)], {
                'Size': chalk.green(c.mergedCount),
                'Authors': chalk.green(c.authorsCount)
            }));
        }

        Formatter.asCompactTable(commitsData)
        return commits;
    }

    static printSummary(month) {
        return (commits) => {
            console.log(format(Formatter.monthSummary(month)(commits)));
        }
    }

    static monthSummary(month) {
        return (commits) => ({
            'Month': month,
            'Total Pull Requests': commits.length,
            'Total commits': commits.reduce(((o, x) => o + x.mergedCount), 0),
            '% of individual': countIndividualWork(commits)
        })
    }

}

module.exports = Formatter;