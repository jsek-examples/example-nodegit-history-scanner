const moment = require('moment');
const {
    Repository,
    Commit
} = require('nodegit');

const config = require('./config.json');
const Utils = require('./utils');

class HistoryScanner {
    constructor() {
        this.ready = Repository.open(config.repositoryPath)
            .then(r => this.Repository = r)
            .then(r => r.getBranchCommit(config.defaultBranch))
            .then(c => this.LastCommit = c)
    }

    scan({
        limit = 100,
        until = new Date(),
        since = new Date('2000-01-01 00:00'),
        filter = () => true
    }) {
        return new Promise((resolve, reject) => {
            this.ready
                .then(() => {
                    let history = this.LastCommit.history();
                    let counter = 0;
                    let commits = [];
                    let finished = false;
                    let end = () => {
                        if (finished) return; // ensure ends only once
                        return resolve(commits);
                    }

                    history.on("commit", commit => {
                        if (counter++ > limit) return end();
                        if (since > commit.date()) return end();
                        if (until >= commit.date() && filter(commit)) commits.push(commit);
                    });

                    history.start();
                })
                .catch(reject);
        });
    }

    scanMonth({
        limit,
        month,
        filter
    }) {
        let since = new Date(`${month}-01 00:00`);
        let until = moment(since).add(1, 'month').toDate();
        return this.scan({
            limit,
            since,
            until,
            filter
        })
    }

    ////////////////////////////////////////////////////////////////////////
    // Retrieve commits added by Pull Request merge - starting from right 
    // side parent
    //
    getMergedCommits(prCommit) {
        return prCommit.parent(1)
            .then(rightParent =>
                this.parentsUntil(rightParent, c => !Utils.isPullRequestMerge(c))
                .then(parentsUntilNextPR => [rightParent].concat(parentsUntilNextPR))
            )
    }


    ////////////////////////////////////////////////////////////////////////
    // Append merged commits statistics 
    //
    withMergedCommits(commits) {
        return Promise
            .all(commits.map(c => this.getMergedCommits(c)))
            .then(results =>
                commits.map(c => {
                    let mergedCommits = results[commits.indexOf(c)];
                    let allAuthors = Utils.distinctAuthors(mergedCommits);
                    // console.log(allAuthors);
                    return Object.assign(c, {
                        mergedCount: mergedCommits.length,
                        authorsCount: allAuthors.length
                    });
                })
            );
    }

    ////////////////////////////////////////////////////////////////////////
    // Recursively take parents until test function returns false or there 
    // are no parents (which did happen, but no idea why)
    // Shortcuts:
    // - choose only one path when someone merged to feature branch
    // 
    // Should be rewritten in the future
    //
    parentsUntil(currentCommit, test) {
        return new Promise((resolve, reject) => {
            return _getParent([], currentCommit, test);

            function _getParent(parents, c, test) {
                if (c.parentcount() < 1) return resolve(parents);
                c.parent(0).then(p => {
                        let next = (c.parentcount() > 1) ?
                            c.parent(1).then(p2 => [p, p2]) :
                            Promise.resolve([p]);
                        next.then(_parents => {
                            let passedParents = _parents.filter(test);
                            if (passedParents.length > 0) {
                                _getParent(parents.concat(passedParents), passedParents[0], test);
                            } else {
                                resolve(parents);
                            }
                        })
                    })
                    .catch(reject);
            }
        });
    }
}

module.exports = HistoryScanner;