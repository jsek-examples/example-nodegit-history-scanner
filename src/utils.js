class Utils {

    static onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    static distinctAuthors(commits) {
        return commits
            .map(c => c.author().name())
            .filter(Utils.onlyUnique);
    }

    static reverse(array) {
        return array.reverse();
    }

    static isPullRequestMerge(commit) {
        return /^PR [0-9]+/.test(commit.message()) && commit.parentcount() === 2;
    }
}

module.exports = Utils;