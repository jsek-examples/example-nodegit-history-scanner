# Example nodegit history scanner

![](images/all-teams.png)

Demo showing analysis on default branch history of a large Git project.

The question was: "Which scenario is more common: **individual** topic branch or **shared** topic branch?". Results are visible on screenshot above.

You can get the list of merge commits for Pull Requests for a chosen timespan or month:

![](images/demo.png)

## Setup

```shell
npm i
npm start
```

Choose an example from `src/index.js`:

```js
example_1();
// example_2();
// example_3();
// example_4();
```

...or write your own. It is super-simple.

## API

First things first:

- navigate to `src/config.json` and adjust **path** and **default branch** name to a repository you would like to analyze

```json
{
    "repositoryPath": "../../some/repo",
    "defaultBranch": "develop"
}
```

### Most basic example

All scan arguments are optional. To get 100 last commits simply pass empty object `{}`;

```js
const HistoryScanner = require('./history-scanner');
const Formatter = require('./formatter');

let scanner = new HistoryScanner();

scanner.scan({
    limit      // [number] safety limit of commits to scan. default: 100
    until      // [Date] limit commits to those made before the date
    since      // [Date] limit commits to those made after the date
    filter     // [function] filter commits to those matching criteria
})
.then(Formatter.asCommitsTable)
.catch(Formatter.printError);
```

### Scanning all commits in given month

The `limit` argument stops scanner after reaching n-th commit since the last one
in `develop` branch. Keep in mind it needs to be large enough, otherwise you won't
get results for some old month.

```js
scanner.scanMonth({
    limit      // [number] safety limit of commits to scan. default: 100
    month      // [string] year and month, e.g. '2017-02'
    filter     // [function] filter commits to those matching criteria
})
.then(Formatter.asCommitsTable)
.catch(Formatter.printError);
```

### Retriving additional statistics about each commit

In `src/index.js` you will find usage of scanner method that attempt to walk down the history graph
looking for commits parents until it finds another Pull Request merge indicating end of commits in
scope of topic branch.

```js
scanner.scan({...})
.then(commits => scanner.withMergedCommits(commits))
.then(...);
```

### Filtering statistics for subset of authors

If you are curious about some group of authors you can narrow commits to perform analysis only on
their commits. Simply specify array of names add following constraint in `filter` function:

```js
scanner.scan({
    filter: commit => team.includes(commit.author().name()) // && some other constraints
})
```

### Getting statistics for multiple months

```js
Promise.all([
        '2016-12',
        '2017-01',
        '2017-02'
    ]
    .map(month =>
        scanner.scanMonth({
            month,
            limit: 10000  // remember to set larger limit
            filter: c => Utils.isPullRequestMerge(c)
        })
        .then(commits => scanner.withMergedCommits(commits))
        .then(Formatter.monthSummary(month))
    ))
.then(Formatter.asCompactTable)
.catch(Formatter.printError);
```
